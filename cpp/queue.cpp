/*
 * queue.cpp
 *
 *  Created on: Sep 22, 2018
 *      Author: mrukant
 */
using namespace std;
#include <iostream>
#include "queue.h"


bool Queue::enqueue(int e)
{

	cout<<endl<<"enqueue";
	cout<<endl<<"rear = "<<rear;
	cout<<" front = "<<front;
	cout<<" e = "<<e;

	if(rear == size)
		return false;
	else
		array[rear++] = e;

	cout<<"   queue["<<rear-1<<"] = "<<array[rear-1]<<endl;

}

int Queue::dequeue()
{

	cout<<endl<<"rear = "<<rear;
	cout<<"front = "<<front;

	if(front == size)
		return -1;
	else
		return array[front++];


}
