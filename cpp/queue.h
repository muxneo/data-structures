/*
 * queue.h
 *
 *  Created on: Sep 16, 2018
 *      Author: mrukant
 */

#ifndef QUEUE_H_
#define QUEUE_H_

class Queue
{
public:
	int* array;
	int front;
	int rear;
	int size;

	Queue():array(NULL),front(0),rear(0),size(0){}
	Queue(int size):front(0),rear(0),size(size)
	{
		array = new int[size];
	}

	bool enqueue(int e);
	int dequeue();
};





#endif /* QUEUE_H_ */
