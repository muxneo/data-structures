using namespace std;
#include <iostream>
#include "linkedlist.h"
#include "stack.h"
#include "queue.h"
#include "circqueue.h"


void printWelcomeMsg()
{
   cout<<endl<<"Welcome to Data Structures Program";
  cout<<endl<<"What would you like to do";
  cout<<endl<<"1) LinkedList";
  cout<<endl<<"2) Stack"; 
  cout<<endl<<"3) Queue";
  cout<<endl<<"4) Check Big/Little Endian";
  cout<<endl<<"5) Reverse Linkedlist";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"1) Stack";
  cout<<endl<<"Enter your choice here : ";
}


bool linkedList()
{
  LinkedList l;

  int e;

  /*Insert and print*/
  for(int i =0 ; i<5;i++)
    {
      cout<<endl<<"insert item"<<endl;
      cin>>e;
      l.insertItem(e);
    }

       l.visitItems();

       /*reverse linked list*/
       l.reverseList();
       l.visitItems();

}

void ReverseWordStack()
{
	//cout<<endl<<"Enter word";
	char s[] = "augmented";
	Stack s1(strlen(s)+1);

	//push data to stack
	  for(int i =0; i<=strlen(s);i++)
	  {
		  s1.push(s[i]);
	  }

	cout<<endl<<"Reversed string is : ";

	for(int i=0;i< s1.maxsize ;i++)
	{
		cout<<s1.pop();
	}




}

void askForResponse2()
{

	cout<<endl<<"Choose more";
	  cout<<endl<<"What would you like to do";
	  cout<<endl<<"1) Reverse Word using Stack";
	  cout<<endl<<"2) tbd";

  int choice;
  cin >> choice;

  switch(choice)
    {
    case 1 : ReverseWordStack();
      break;
    /*case 2 : stack();
      break;
    case 3 : queue();
      break;
      default: stack();*/

    }
}


bool stack()
{
  Stack s(10);

  //push data to stack
  for(int i =0; i<10;i++)
  {
	  s.push(i);
  }

  //print and pop data from stack
  for(int i =0; i<10;i++)
    {
	  cout<<endl<<s.pop();
    }

  askForResponse2();

}


void circularqueue()
{
	CircQueue cq(10);

	//Add data to queue
		for(int i =0; i < cq.size ; i++)
		{
			cq.enqueue(i);
		}

		//print and remove data from queue
		for(int i = 0;i< cq.size ; i++)
		{
			cout<<endl<<"Queue data :";
			cout<<endl<< cq.dequeue()<<endl;
		}


}

void askforresponse3()
{
	cout<<endl<<"Choose more";
		  cout<<endl<<"What would you like to do";
		  cout<<endl<<"1) Circular Queue";
		  cout<<endl<<"2) tbd  :"<< endl;

	  int choice;
	  cin >> choice;

	  switch(choice)
	    {
	    case 1 : circularqueue();
	      break;
	    /*case 2 : stack();
	      break;
	    case 3 : queue();
	      break;
	      default: stack();*/

	    }
}

bool queue()
{
	Queue q(10);

	//Add data to queue
	for(int i =0; i < q.size ; i++)
	{
		q.enqueue(i);
	}

	//print and remove data from queue
	for(int i = 0;i< q.size ; i++)
	{
		cout<<endl<<"Queue data :";
		cout<<endl<< q.dequeue()<<endl;
	}

	askforresponse3();

}

void biglittle()
{
  int i = 0x01;
  if(((i>>8) & 0b1) == 0b1)
    cout<<"Big"<<endl;
  else
    cout<<"Little"<<endl;

  cout<<"Printing representation of 0x01234567 in machine"<<endl;
  int x = 0x01234567;
  char * charrepr = (char*) &x;
  cout<<std::hex<<(int)*charrepr<<" ";
  charrepr++;
  cout<<std::hex<<(int)*charrepr<<" ";
  charrepr++;
  cout<<std::hex<<(int)*charrepr<<" ";
  charrepr++;
  cout<<std::hex<<(int)*charrepr<<" ";
}

void reverseLLSetup(LinkedList* l)
{
  for(int i=0;i<10;i++)
    {
      l->insertItem(i);
    }

  cout<<endl<<"Reverse LinkedList demo"<<endl;
  cout<<"Current LinkedList :";

      l->traverse();
    
}

void reverseLinkedList()
{
  LinkedList l;
  reverseLLSetup(&l);
  Node *prev, *curr, *next;

  prev=curr=next=l.getHead();

  curr=next=curr->next;
  prev->next = NULL;

  while(curr != NULL)
  {
    if(curr->next != NULL)
      next = curr->next;
    else
    {
    	curr->next = prev;
    	break;
    }


    curr->next = prev;
    prev = curr;
    curr = next;
  }

  l.setHead(curr);

  
  cout<<endl<<"Reversed LinkedList : ";
     l.traverse();
}

void askForResponse()
{
  int algono;
  cin >> algono;

  switch(algono)
    {
    case 1 : linkedList();
      break;
    case 2 : stack();
      break;
    case 3 : queue();
      break;
    case 4 : biglittle();
      break;
    case 5 : reverseLinkedList();
      break;
      /*default: stack();*/
      
    }
}





int main()
{

  printWelcomeMsg();
  askForResponse();
 
}
