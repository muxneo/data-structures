/*
 * node.h
 *
 *  Created on: Sep 8, 2018
 *      Author: mux
 */

#ifndef NODE_H_
#define NODE_H_


class Node
{
 public:
  int element;
  Node* next;

 Node() : element(0),next(0) {}
 Node(int e):element(e),next(0) {}
};



#endif /* NODE_H_ */
