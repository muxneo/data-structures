/*
 * Stack.h
 *
 *  Created on: Sep 15, 2018
 *      Author: mrukant
 */

#ifndef STACK_H_
#define STACK_H_

#include <cstdio>
#include <cstring>

class Stack
{
	public:
		char* arr;
		int top;
		int maxsize;

		Stack():top(-1),maxsize(0),arr(0){}

		Stack(int size):top(-1),maxsize(size)
		{
			arr = new char[maxsize];
		}

		Stack(char* str):top(-1)
		{
			maxsize = strlen(str) + 1;
			arr = new char(maxsize);
			sprintf(arr,"%s",str);
		}

		bool push(char e);
		char pop();


};



#endif /* STACK_H_ */
