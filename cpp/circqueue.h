/*
 * CircQueue.h
 *
 *  Created on: Sep 22, 2018
 *      Author: mrukant
 */

#ifndef CIRCQUEUE_H_
#define CIRCQUEUE_H_

class CircQueue
{
public:
	int* array;
	int front;
	int rear;
	int size;
	int totalelements;

	CircQueue():array(NULL),front(0),rear(0),size(0),totalelements(0){}
	CircQueue(int size):front(0),rear(0),size(size),totalelements(0)
	{
		array = new int[size];
	}

	bool enqueue(int e);
	int dequeue();

};




#endif /* CIRCQUEUE_H_ */
