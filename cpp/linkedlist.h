/*
 * linkedlist.h
 *
 *  Created on: Sep 8, 2018
 *      Author: mux
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include <iostream>
#include "node.h"


class LinkedList
{
private:
  Node* first;

public:
  LinkedList()
  {
  	first = new Node();
  }

  Node* getHead();
  void setHead(Node* n);
  void insertItem(int e);
  void visitItems();
  bool deleteFirstItem(int e);
  void traverse();
  int findFirst(int e);
  Node* reverseList();
};




#endif /* LINKEDLIST_H_ */
